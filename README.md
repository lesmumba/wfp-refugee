Smart Business Networks Group 9 Prototype
====
Setup
----
This prototype is built using the following frameworks.

1. [Embark](https://embark.status.im/)
2. [jQuery](https://jquery.com/)
3. [Bootstrap](http://getbootstrap.com/)
4. [Popper.js](https://popper.js.org/)
4. [PouchDB](https://pouchdb.com/)
5. [Exif.js](https://github.com/exif-js/exif-js)

In a terminal, switch to the folder you cloned the prototype from and run the following commands.
```npm -g install embark```

```npm install jquery```

```npm install bootstrap```

```npm install popper.js```

```npm install PouchDB```

Launch webserver and blockchain
----
Once these dependencies are met, you can run the webserver by running the following code in two separate terminals.

```embark run ```

```embark blockchain```

Testing
----
The prototype uses the date modified value of the image named *01_iris_1_a.jpg* in the /app/images folder. It is possible to use the prototype with another image, but some additional effort is required.

The first time you launch the prototype, it is important to press the button which says "Seed Refugees." This creates an refugee in our blockchain which is tied to our sample image. Even though it does not look like anything happened, a block should have been created. You can check the console in which you ran you `embark blockchain` to confirm this.

After seeding a refugee, press the scan retina button and browse to the test image provided.