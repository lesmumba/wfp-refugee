pragma solidity ^0.4.23;

contract Refugee {

    event BasicAccess(address indexed organization);

    address public owner;
    string public _first_name;
    string public _last_name;
    string public _dob;
    string private _nationality;
    struct Organization {
        bool medicalAccess;
        bool schoolAccess;
        bool fullAccess;
        bool isSet;
        address organization;
    }
    mapping(address => Organization) private organizations;

    constructor(string first_name, string last_name, string date_of_birth, string nationality) public {
        _first_name = first_name;
        _last_name = last_name;
        _dob = date_of_birth;
        _nationality = nationality;
    }

    function getDoB(address accessor) public view returns (string dob) {
        return _dob;
    }

    // It would be better to return a data structure here which includes medical, school and nationality.
    // Solidity does not support this as yet.
    function getNationality(address organization) public view returns (string nationality) {
        Organization storage org_ = organizations[organization];
        if (org_.fullAccess) {
            return _dob;
        } else {
            return "Permission Denied";
        }
    }

    // Best to return an array here, but solidity does not support this as yet.
    function getMedicalRecords(address organization) public view returns (bool ok) {
        Organization storage org = organizations[organization];
        if (org.medicalAccess || org.fullAccess) {
            return true;
        } else {
            return false;
        }
    }

    // Best to return an array here, but solidity does not support this as yet.
    function getSchoolRecords(address organization) public view returns (bool ok) {
        Organization storage org = organizations[organization];
        if (org.schoolAccess || org.fullAccess) {
            return true;
        } else {
            return false;
        }
    }

    function grantMedicalAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.medicalAccess = true;
        return true;
    }

    function revokeMedicalAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.medicalAccess = false;
        return false;
    }

    function grantSchoolAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.schoolAccess = true;
        return true;
    }

    function revokeSchoolAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.schoolAccess = false;
        return false;
    }

    function grantFullAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.fullAccess = true;
        return org_.fullAccess;
    }

    function revokeFullAccess(address organization) public returns (bool ok) {
        checkOrg(organization);
        Organization storage org_ = organizations[organization];
        org_.fullAccess = false;
        return true;
    }

    function checkOrg(address orgAddress) internal {
        Organization storage org_ = organizations[orgAddress];
        if (!org_.isSet) {
            organizations[orgAddress] = Organization(false, false, false, true, orgAddress);
        }
    }
}