import EmbarkJS from 'Embark/EmbarkJS';
import 'bootstrap';
import PouchDB from 'pouchdb';
import * as PouchDBFind from 'pouchdb-find';
import EXIF from 'exif-js';
import $ from 'jquery';

import Refugee from 'Embark/contracts/Refugee';

import 'bootstrap/dist/css/bootstrap.min.css';

// import your contracts
// e.g if you have a contract named SimpleStorage:
//import SimpleStorage from 'Embark/contracts/SimpleStorage';

// PouchDB.plugin(import PouchDBFind);
var db = new PouchDB('refugees');

var currentAccount;
var accessLevelRequest;
var refugee;

$(document).ready(function() {

	$("#requestAccessDiv").hide();
	$("#confirmAccess").hide();

	// Get Account 0. In the future there will be some kind of session management to handle this.
	web3.eth.getAccounts(function(err, accounts) {
		currentAccount = accounts[0];
	});

	// Get all refugees. This was done for logging purposes.
	db.allDocs({include_docs: true}).then(function (result) {
		console.log($.isEmptyObject(result.rows));
		var tempToken;
		$.each(result.rows, function() {
			var docId = this.doc._id;
			console.log(docId);
		});
	});

	$("#iris_scan").change(function() {
		if (this.files && this.files[0]) {
	        var reader = new FileReader();
	        var placeholder = this+"-placeholder";
	        var latitudePlaceholder = this+"-latitude";
	        var longitudePlaceholder = this+"-longitude";

	        reader.onload = function (e) {
	            $("#iris_scan_image").attr('src', e.target.result);
	        }
	        reader.readAsDataURL(this.files[0]);

	        // Read last modified value from the image scanned
	        // Use last modified value to query the table of refugees for the address.
	        var iris_metadata = String(this.files[0].lastModified);
	        db.get(iris_metadata).then(function(doc){
	        	refugee = new EmbarkJS.Contract({
					abi: Refugee.options.jsonInterface,
					address: doc.address
				});
				// Small test to make sure we're getting information from the blockchain.
				refugee.methods._first_name().call().then(function(res) {
					console.log(res);
				});
				console.log("Done testing.");
				
				// Get the date of birth from the refugee just found.
				refugee.methods.getDoB(currentAccount).call().then(function(dob) {
					console.log('Getting DOB');
					console.log(dob);
					var parsedDate = new Date(0);
					parsedDate.setUTCSeconds(dob);
					console.log(parsedDate.toLocaleDateString());
					var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
					$("#infoTable_dob").text(parsedDate.toLocaleDateString("en-GB", options));
				});

				// Test the getNationality method.
				console.log(currentAccount);
				refugee.methods.getNationality(currentAccount).call().then(function(nationality) {
					console.log("Nationality: " + nationality);
				});

				// Update the refugee information area.
	        	$("#infoTable_name").text(doc.first_name + " " + doc.last_name);
	        	// Disable the scan retina button.
	        	$("#iris_scan").attr("disabled", true);
	        	$("#iris_scan_cover").removeClass("btn-success");
	        	$("#iris_scan_cover").addClass("btn-secondary");
	        	// Show the next bit.
	        	$("#requestAccessDiv").show("slow");
	        })

	        console.log(this.files[0].lastModified);

	        // "this" here is referring to the file input
		    EXIF.getData(this.files[0], function() {
		        var latitude = EXIF.getTag(this, "GPSLatitude");
		        if (latitude === undefined) {
		        	console.log("Problem getting latitude");
		        } else {
		        	latitude = latitude[0].numerator + latitude[1].numerator /
		        		(60 * latitude[1].denominator) + latitude[2].numerator / (3600 * latitude[2].denominator);
		        	console.log(latitude);
		        }
		        var longitude = EXIF.getTag(this, "GPSLongitude");
		        if (longitude === undefined) {
		        	console.log("Problem getting longitude");
		        } else {
		        	longitude = longitude[0].numerator + longitude[1].numerator /
		        		(60 * longitude[1].denominator) + longitude[2].numerator / (3600 * longitude[2].denominator);
		        	console.log(longitude);
		        }
	      	});
	    }
	});

	$("#requestAccessButton").click(function() {
		var accessRequestText;
		// Get the level requested.
		accessLevelRequest = $('input[name=accessRadios]:checked').val();
		switch (accessLevelRequest) {
			case 'school':
				accessRequestText = "This organization is requesting access to your school history";
				break;
			case 'medical':
				accessRequestText = "This organization is requesting access to your medical history";
				break;
			case 'full':
				accessRequestText = "This organization is requesting full access to your identity. Including your; nationality, school and medical history";
				break;
			default:
				accessRequestText = "N/A";
		}
		// Hide the request dialog.
		$("#requestAccessDiv").hide("slow");
		$("#accessRequestText").text(accessRequestText);
		// Show the confirmation screen.
		$("#confirmAccess").show();
	})

	$("#grantAccess").click(function() {
		var requestStatus;
		switch (accessLevelRequest) {
			case 'medical':
				refugee.methods.grantMedicalAccess(currentAccount).call().then(function(res) {
					console.log(res);
					if (res == true) {
						$("#accessGrantedModal").modal();
					} else {
						$("#accessRevokedModal").modal();
					}
				});
				break;
			case 'school':
				refugee.methods.grantSchoolAccess(currentAccount).call().then(function(res) {
					console.log(res);
					if (res == true) {
						$("#accessGrantedModal").modal();
					} else {
						$("#accessRevokedModal").modal();
					}
				});
				break;
			case 'full':
				refugee.methods.grantFullAccess(currentAccount).call().then(function(res) {
					console.log(currentAccount);
					console.log(res);
					if (res == true) {
						$("#accessGrantedModal").modal();
					} else {
						$("#accessRevokedModal").modal();
					}
				});
				refugee.methods.getNationality(currentAccount).call().then(function(nationality) {
					console.log(nationality);
				});
				break;
			default:
				requestStatus = false;
				break;
		}
	});

	// This function seeds one refugee based on the date modified metadata of the picture we plan to provide.
	// Important to click the associated button that triggers this whenever a change is made to the contract.
	$("#seedRefs").click(function(){
		// 1527508234000 = date modified of 01_iris_1_a.jpg
		// 1527508235000 = data modified of IMG_20180528_134920.jpg
		var first_name = "Alina";
		var last_name = "Varvashenia";
		var dob = "1527506994";
		var nationality = "Belarus";
		Refugee.deploy({data : Refugee.options.data, arguments:[first_name, last_name, dob, nationality]}).send({gas: 4000000}).then(function(deployedToken) {
			db.get('1527508234000').then(function(doc) {
				// Delete the previous entry in the database for our refugee.
				return db.remove(doc);
			}).then(function (result) {
				db.put({
					_id: "1527508234000",
					first_name: first_name,
					last_name: last_name,
					address: deployedToken.options.address,
					iris_info: "1527508234000"
				});
			}).catch(function (err) {
			  console.log(err);
			});
		});
	})
});